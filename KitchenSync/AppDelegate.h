//
//  AppDelegate.h
//  KitchenSync
//
//  Created by cetauri on 12. 12. 17..
//  Copyright (c) 2012년 Baas.io. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
